import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            result: 0
        }
        this.addOne = this.addOne.bind(this);
        this.minusOne = this.minusOne.bind(this);
        this.multiplyTwo = this.multiplyTwo.bind(this);
        this.divideTwo = this.divideTwo.bind(this);
    }

    addOne(event) {
        console.log(event.target.value);
        let tmp = Number.parseFloat(this.state.result) + Number.parseFloat(event.target.value);
        this.setState({result: tmp});
    }

    minusOne(event) {
        let tmp = Number.parseFloat(this.state.result) - Number.parseFloat(event.target.value);
        this.setState({result: tmp});
    }

    multiplyTwo(event) {
        let tmp = Number.parseFloat(this.state.result) * Number.parseFloat(event.target.value);
        this.setState({result: tmp});
    }

    divideTwo(event) {
        let tmp = Number.parseFloat(this.state.result) / Number.parseFloat(event.target.value);
        this.setState({result: tmp});
    }



  render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.result} </span>
        </div>
        <div className="operations">
          <button onClick={this.addOne} value={1}>加1</button>
          <button onClick={this.minusOne} value={1}>减1</button>
          <button onClick={this.multiplyTwo} value={2}>乘以2</button>
          <button onClick={this.divideTwo} value={2}>除以2</button>
        </div>
      </section>
    );
  }
}

export default MiniCalculator;

